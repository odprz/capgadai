import { Injectable } from '@angular/core';
import { Developer } from '../models/developer';

@Injectable({
  providedIn: 'root'
})
export class DevServiceService {

  listDevs:  Developer [];

  constructor() {
    this.listDevs = [
      {Id: 1, Nombre: 'Raymundo', Apodo: 'Garbanzo', Proyecto: 'SIMOSICA' },
      {Id: 2, Nombre: 'Juan Carlos', Apodo: 'Mijis', Proyecto: 'GRP 3.0' },
      {Id: 3, Nombre: 'Issac', Apodo: 'Wajis', Proyecto: 'Presupuesto' },
      {Id: 4, Nombre: 'Pedro', Apodo: 'Pedrito', Proyecto: 'SIMOSICA' },
      {Id: 5, Nombre: 'Ricardo', Apodo: 'Quesito', Proyecto: 'PPMA' }
    ];
  }

  // lista de developers
  getDevelopers (): Developer[] {
    return this.listDevs;
  }

  // regresa developer por id
  getDeveloperByID(id: number): Developer {
    return this.listDevs.find( x => x.Id === id );
   }

   // edita un developer  del listado buscandolo por su identificador
   putDeveloper(id: number, developer: Developer ) {

    this.listDevs.splice(id - 1 , 1, developer );

   }

   // agrega un developer al listado
   postDeveloper( developer: Developer) {
     this.listDevs.push(developer);
   }

   // elimina un developer del listado
   deleteDeveloper(id: number ) {

    this.listDevs.splice(id - 1 , 1);

   }



}
