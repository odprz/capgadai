import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DxButtonModule, DxDataGridModule, DxCheckBoxModule, DxSelectBoxModule, DxNumberBoxModule, DxFormModule } from 'devextreme-angular';

import { DevServiceService } from './services/dev-service.service';

import { HeaderComponent } from './components/shared/header/header.component';
import { AppComponent } from './app.component';
import { DevelopersComponent } from './components/developers/developers.component';
import { EditarDevComponent } from './components/editar-dev/editar-dev.component';
import { NuevoDevComponent } from './components/nuevo-dev/nuevo-dev.component';
import { AppRouting } from './app.routing';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DevelopersComponent,
    EditarDevComponent,
    NuevoDevComponent,
  ],
  imports: [
    BrowserModule,
    DxButtonModule,
    DxDataGridModule,
    AppRouting,
    DxCheckBoxModule,
    DxSelectBoxModule,
    DxNumberBoxModule,
    DxFormModule
  ],
  providers: [DevServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
