import { Component, OnInit } from '@angular/core';
import { DevServiceService } from 'src/app/services/dev-service.service';
import { Developer } from 'src/app/models/developer';
import { Router } from '@angular/router';

@Component({
  selector: 'app-developers',
  templateUrl: './developers.component.html',
  styleUrls: ['./developers.component.scss']
})
export class DevelopersComponent implements OnInit {

  constructor(
    private devService: DevServiceService,
    private router: Router
    ) {

   }

   listDevs: Developer[] = [];

  ngOnInit() {
    this.listDevs = this.devService.getDevelopers();
    console.log('Listado de devs', this.listDevs );
  }

  deletDev(info: any) {
    console.log('info delete', info.data);
    this.devService.deleteDeveloper(info.data.Id);
  }

  editDev( info: any) {
    this.router.navigate(['edit', info.data.Id]);
  }
}
