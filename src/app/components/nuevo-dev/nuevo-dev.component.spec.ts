import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoDevComponent } from './nuevo-dev.component';

describe('NuevoDevComponent', () => {
  let component: NuevoDevComponent;
  let fixture: ComponentFixture<NuevoDevComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoDevComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoDevComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
