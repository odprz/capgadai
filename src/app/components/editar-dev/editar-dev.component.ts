import { Component, OnInit } from '@angular/core';
import { DevServiceService } from 'src/app/services/dev-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Developer } from 'src/app/models/developer';

@Component({
  selector: 'app-editar-dev',
  templateUrl: './editar-dev.component.html',
  styleUrls: ['./editar-dev.component.scss']
})
export class EditarDevComponent implements OnInit {

  constructor(
    private devService: DevServiceService,
    private route: ActivatedRoute,
    private router: Router
    ) { }
    id: number;
    selectedDev: Developer;

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.selectedDev = this.devService.getDeveloperByID(Number(this.id));
    console.log('developer seleccionado: ', this.selectedDev);
  }

  guardar(e: any) {
    e.preventDefault();
    this.devService.putDeveloper(Number(this.id), this.selectedDev);
    this.router.navigate(['']);
  }

}
