import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

// import { HomeComponent } from './';
import { DevelopersComponent } from './components/developers/developers.component';
import { EditarDevComponent } from './components/editar-dev/editar-dev.component';
import { NuevoDevComponent } from './components/nuevo-dev/nuevo-dev.component';
// import { Name2Component } from './';
// import { Name3Component } from './';
// import { Name4Component } from './';
// import { PageNotFoundComponent } from './';

const ROUTES: Routes = [
  { path: '', component: DevelopersComponent },
  { path: 'developers', component: DevelopersComponent },
  { path: 'nuevo', component: NuevoDevComponent },
  { path: 'edit/:id', component: EditarDevComponent },
  // { path: 'path3', component: Name3Component },
  // { path: 'path4', component: Name4Component },
  { path: '**', component: DevelopersComponent },

  // { path: 'path/:routeParam', component: MyComponent },
  // { path: 'staticPath', component: ... },
  // { path: '**', component: ... },
  // { path: 'oldPath', redirectTo: '/staticPath' },
  // { path: ..., component: ..., data: { message: 'Custom' }
];

// @NgModule({
//   imports: [RouterModule.forChild(routes)],
//   exports: [RouterModule]
// })
export const AppRouting = RouterModule.forRoot(ROUTES);
